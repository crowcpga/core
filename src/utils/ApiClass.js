class ApiClass {
  // endpoint = null;
  constructor(sendRequest) {
    this.sendRequest = sendRequest;
  }
  async all() {
    const response = await this.sendRequest(this.endpoint);
    return response;
  }
  async create(data) {
    const response = await this.sendRequest(
      `${this.endpoint}`, // url
      "POST", // method
      JSON.stringify(data), // body
      { "Content-Type": "application/json" } // header
    );
    return response;
  }
  async read(id) {
    const response = await this.sendRequest(`${this.endpoint}/${id}`);
    return response;
  }
  async update(id, data) {
    const response = await this.sendRequest(
      `${this.endpoint}/${id}`, // url
      "PUT", // method
      JSON.stringify(data), // body
      { "Content-Type": "application/json" } // header
    );
    return response;
  }
  async delete(id) {
    const response = await this.sendRequest(
      `${this.endpoint}/${id}`, // url
      "DELETE" // method
    );
    return response;
  }
}

export default ApiClass;
