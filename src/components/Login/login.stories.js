// /src/components/Button/button.stories.js
import React from "react";
import Login from "./index.js";
import { withKnobs, text, boolean } from "@storybook/addon-knobs";

export default { title: "Login component", decorators: [withKnobs] };

export const login = () => {
  const loginInputGroup = "Login Inputs";
  const loginButtonGroup = "Login Button";
  const username = text("Username", "username...", loginInputGroup);
  const password = text("Password", "password...", loginInputGroup);
  return (
    <Login
      username={username}
      password={password}
      disabledButton={boolean("disabled", false, loginButtonGroup)}
      onSubmitHandler={(e) => {
        alert(`Username: ${username}, Password: ${password}`);
        e.preventDefault();
        return false;
      }}
    />
  );
};
