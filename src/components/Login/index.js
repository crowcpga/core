import React from "react";

const Login = ({
  username,
  password,
  disabledButton = false,
  onSubmitHandler,
}) => (
  <form onSubmit={onSubmitHandler}>
    <input placeholder="username" value={username} autoComplete="off" />
    <input
      placeholder="password"
      value={password}
      type="password"
      autoComplete="off"
    />
    <button disabled={disabledButton}>Login</button>
  </form>
);

export default Login;
