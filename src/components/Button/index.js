// /src/components/Button/index.js
import React from "react";

const Button = ({ message = "Hello world", ...props }) => (
  <button {...props}>{message}</button>
);
export default Button;
