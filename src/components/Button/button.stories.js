// // /src/components/Button/button.stories.js
// import React from "react";
// import Button from "./index.js";
// import { withKnobs, text } from "@storybook/addon-knobs";
// export default { title: "Button component", decorators: [withKnobs] };
// export const button = () => {
//   const message = text("Text", "Click here now!");
//   return <Button message={message}></Button>;
// };

import React from "react";
import {
  withKnobs,
  text,
  boolean,
  number,
  color,
  object,
  array,
  select,
} from "@storybook/addon-knobs";
import Button from "./index";

export default {
  title: "Storybook Knobs",
  decorators: [withKnobs],
};
// Add the `withKnobs` decorator to add knobs support to your stories.
// You can also configure `withKnobs` as a global decorator.

// Knobs as dynamic variables.
export const asDynamicVariables = () => {
  const name = text("Name", "James");
  const age = number("Age", 37, {
    range: true,
    min: 0,
    max: 37,
    step: 1,
  });
  const c = color("Color", "#ffffff");
  const json = object("JSON", {});
  const content = `I am ${name} and I'm ${age} years old.`;

  const styles = array("Styles", ["Red", "Blue"], ":");

  const _select = select(
    "select",
    {
      Red: "red",
      Blue: "blue",
      Yellow: "yellow",
      Rainbow: ["red", "orange", "etc"],
      None: null,
    },
    "red"
  );

  return (
    <div style={{ background: c }}>
      {content}
      <pre>{styles}</pre>
      <pre>{JSON.stringify(json, null, 4)}</pre>
      <pre>{JSON.stringify(_select, null, 4)}</pre>
    </div>
  );
};

export const asButton = () => {
  return (
    <Button
      onClick={(e) => alert(text("Message"))}
      message={text("Message", "Hello World")}
      disabled={boolean("Disabled", false)}
    ></Button>
  );
};
