# Como instalar con NPM

```
npm install @codenoga/core --repository http://package.gitbox.cl
```

# Como instalar con Yarn

### agrega el repositorio en la configuracion de yarn

```
// agrega la siguiente linea al archivo: ~/.yarnrc
registry "http://package.gitbox.cl"
// luego
yarn install @codenoga/core
```

# Implementacion del hook useHttpClient

**App.js**

```
import React, { useState, useEffect, useCallback, Fragment } from "react";
import { useHttpClient } from "@codenoga/core";

function App() {
  const [response, setResponse] = useState();
  const { isLoading, sendRequest } = useHttpClient();

  // useCallback
  const fetchData = useCallback(async () => {
    setResponse(
      await sendRequest("https://jsonplaceholder.typicode.com/posts")
    );
  }, [sendRequest]);

  // useEffect
  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <Fragment>
      {isLoading ? (
        <span>Loading ...</span>
      ) : (
        <Fragment>
          <pre>{JSON.stringify(response, null, 4)}</pre>
        </Fragment>
      )}
    </Fragment>
  );
}

export default App;
```

# Implementacion del componente API, realizando herencia de clases.

**APIPosts.js**

```
// APIPosts.js
import { ApiClass } from "@codenoga/core";
export default class APIPosts extends ApiClass {
  endpoint = "https://jsonplaceholder.typicode.com/posts";
}
```

**App.js**

```
// App.js
import React, { useState, useEffect, useCallback, Fragment } from "react";
import { useHttpClient } from "@codenoga/core";
import APIPosts from "./APIPosts";

function App() {
  const [response, setResponse] = useState();
  const { isLoading, sendRequest } = useHttpClient();

  // useCallback
  const fetchData = useCallback(async () => {
    const api = new APIPosts(sendRequest);
    setResponse(await api.all());
  }, [sendRequest]);

  // useEffect
  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <Fragment>
      {isLoading ? (
        <span>Loading ...</span>
      ) : (
        <pre>{JSON.stringify(response, null, 4)}</pre>
      )}
    </Fragment>
  );
}

export default App;

```

# Implementación del componente Login

**App.js**

```
// App.js
import React, { useState } from "react";
import { Login } from "@codenoga/core";

function App() {
  const [username] = useState("Username ...");
  const [password] = useState("Password...");
  return (
    <Login
      username={username}
      password={password}
      onSubmitHandler={(e) => {
        alert(`Username: ${username}, Password: ${password}`);
        e.preventDefault();
      }}
    />
  );
}

export default App;
```
